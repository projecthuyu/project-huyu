<?php
require_once('template/header.php');

?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Search for Facebook User by Node Number</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" name="customerFinder" onSubmit="<?php echo $_SERVER['PHP_SELF'];?>">
						<div class="form-group">
							<label for="searchString">Facebook Node Number</label>
							<input type="text" class="form-control" name="searchString" id="searchString" placeholder="Enter Facebook Node Number">
						</div>
						<input type="submit" name="Search" id="Search" class="btn btn-info" onClick="document.pressed=this.value" value="Search">
					</form>
				</div>
			</div>
<?php
require_once('template/footer.php');